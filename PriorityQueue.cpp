#include "PriorityQueue.h"
#include <iostream>

void PriorityQueue::addJob(string item, int procs, int ticks)
{
    job *temp, *q;

    temp = new job;
    temp->jobname = item;
    temp->processors = procs;
    temp->job_ticks = ticks;

    //whether queue is empty
    if (front == NULL || ticks < front->job_ticks)
    {
        temp->next = front;
        front = temp;
    }else
        {
            q = front;
            while (q->next != NULL && q->next->processors <= processors)
                q = q->next;
            temp->next = q->next;
            q->next = temp;
        }
}

void PriorityQueue::printJob()
{
    job *temp;

    if (front == NULL)
        cout << "No jobs in queue.\n";
    else{
        temp = front;
        cout << temp->jobname << endl;
        if( front.job_ticks <= 0)  // deletes the job if it has 0 ticks left
        {
            front = front->next;
            free (temp);   
        } 
        }   
}

void PriorityQueue::viewJob()
{
    job *ptr;
    ptr = front;

    if (front == NLL)
        cout << "No jobs in queue.\n";
    else{
        while (ptr != NULL)
        {
            cout << ptr->jobname << endl;
            ptr = ptr->next;
        }
    }
}

void PriorityQueue::empty()
{
    job *ptr;
    ptr = front;
    if (ptr != NULL)
        return 1; //not empty
    return 0; //empty
}

void PriorityQueue::decrementTick()
{
    job *ptr;
    ptr = front;
    while (ptr->next != NULL)
    {
        ptr->job_ticks--;
        ptr = ptr->next;
    }
}