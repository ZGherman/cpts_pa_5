#include "Heap.h"
#include <string>

using namespace std;


//node of the Priority Queue
struct job
{
    //declare variables
    int processors;
    int job_ticks;
    string jobname;
    job *next;
};

//Priority Queue
class PriorityQueue
{
    private:
    job *front;
    //constructor
    public:
    PriorityQueue()
    {
        front = NULL;
    }

    void PriorityQueue::addJob(string item, int procs, int ticks);
    void PriorityQueue::printJob();
    void PriorityQueue::viewJob();
    int PriorityQueue::empty();
    void PriorityQueue::decrementTick();
};