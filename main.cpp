//CPTS_223_PA5
#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include "PriorityQueue.h"
#include "Heap.h"

using namespace std;

int main()
{
    //declare variables
    int ticks_User, procs_User, choice, tot_Procs;
    PriorityQueue pq;
    MinHeap h;
    char ch;
    string jobname;
    int number = 0;

    cout << "How many processors will we be working with?"
    cin >> tot_Procs;

    cout << "Job Queue" << endl;

    do{
        // prompt and read input from the user
        cout << "\n1.Add Job\n";
        cout << "\n2.Print Running Jobs\n";
        cout << "\n3.Print Waitlisted Jobs\n";
        cout << "\n4.Exit\n";
        cin >> choice;
        int *arr;

        switch (choice)
        {
            // decrement tick on all items
            if (pq.empty() == 1)
            {
                pq.decrementTick();
                //find shortest
                //delete shortest
                //release procs
            }

            // If the user adds a job, then the program asks the user
            // the job name, ticks and how many processors it will use.
            case 1:
                cout << "\nWhat is the name of this job?\n";
                cin >> jobname;
                cout << "\nHow many processors will it use?\n";
                cin >> procs_User;
                cout << "\nHow many ticks will it run for?\n";
                cin >> ticks_User;

                arr[number] = procs_User;
                number++;
                pq.addJob(jobname, procs_User, ticks_User);
                h.insertKey(number);
                h.HeapSort(arr, number);

                break;

            case 2:
                // print running jobs
                break;
            case 3:
                // print waiting jobs
                break;
            case 4:
                break;
            default:
                cout << "\nInvalid choice\n";
        }   
    }

    //Exit the program
    while (choice != 4);
    //pause the system for a moment
    system("PAUSE");
    return 0;
}