Zach Gherman
CPTS_223

I got lost in my code and cant figure out how to configure the intellisense to include the proper pathways
that I need to get my code to run. I know I will likely get 0 points but atleast I have shown something and
put in the effort to actually create the project.
As for me getting lost, I got lost in how to manipulate the priority queue to do the actions that are required
for this assignment. 

Report:
I believe some of the short comings of using a shortest-job-first strategy is that you have to completely rebalance
the heap after you remove any item with the shortest time. It adds time which hinders the performance but for 
functionality it is a good choice. Since it allows the user quick access to the items with the shortest time
and makes removing a O(1) runtime since it doesn't have to search.